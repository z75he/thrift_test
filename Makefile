CC=g++

LIBS=-lthrift -lthriftnb -levent

COMMON_SOURCE=PerformanceTest.cpp test_rpc_types.cpp

SERVER_SOURCE=PerformanceTest_server.cpp 
CLIENT_SOURCE=test_rpc_client.cpp


# default
.DEFAUT_GOAL = DEFAULT

DEFAULT: server client

client: $(CLIENT_SOURCE) $(COMMON_SOURCE)
	$(CC) $(CLIENT_SOURCE) $(COMMON_SOURCE) -o client $(LIBS) -lpthread

server: $(SERVER_SOURCE) $(COMMON_SOURCE)
	$(CC) $(SERVER_SOURCE) $(COMMON_SOURCE) -o server $(LIBS)

clean:
	/bin/rm -f server client