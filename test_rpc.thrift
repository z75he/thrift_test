struct TEST_PACKET_SIMPLE {
    1: i64 test_id,
    2: list<i32> payload
}


service PerformanceTest
{
    i64 simple_return_int(1: TEST_PACKET_SIMPLE arg1),

    TEST_PACKET_SIMPLE int_return_simple(1: i64 id),

    TEST_PACKET_SIMPLE simple_return_simple(1: TEST_PACKET_SIMPLE arg1)
}