/**
 * Autogenerated by Thrift Compiler (0.14.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef test_rpc_TYPES_H
#define test_rpc_TYPES_H

#include <iosfwd>

#include <thrift/Thrift.h>
#include <thrift/TApplicationException.h>
#include <thrift/TBase.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TTransport.h>

#include <functional>
#include <memory>




class TEST_PACKET_SIMPLE;

typedef struct _TEST_PACKET_SIMPLE__isset {
  _TEST_PACKET_SIMPLE__isset() : test_id(false), payload(false) {}
  bool test_id :1;
  bool payload :1;
} _TEST_PACKET_SIMPLE__isset;

class TEST_PACKET_SIMPLE : public virtual ::apache::thrift::TBase {
 public:

  TEST_PACKET_SIMPLE(const TEST_PACKET_SIMPLE&);
  TEST_PACKET_SIMPLE& operator=(const TEST_PACKET_SIMPLE&);
  TEST_PACKET_SIMPLE() : test_id(0) {
  }

  virtual ~TEST_PACKET_SIMPLE() noexcept;
  int64_t test_id;
  std::vector<int32_t>  payload;

  _TEST_PACKET_SIMPLE__isset __isset;

  void __set_test_id(const int64_t val);

  void __set_payload(const std::vector<int32_t> & val);

  bool operator == (const TEST_PACKET_SIMPLE & rhs) const
  {
    if (!(test_id == rhs.test_id))
      return false;
    if (!(payload == rhs.payload))
      return false;
    return true;
  }
  bool operator != (const TEST_PACKET_SIMPLE &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const TEST_PACKET_SIMPLE & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

  virtual void printTo(std::ostream& out) const;
};

void swap(TEST_PACKET_SIMPLE &a, TEST_PACKET_SIMPLE &b);

std::ostream& operator<<(std::ostream& out, const TEST_PACKET_SIMPLE& obj);



#endif
