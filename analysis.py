import pandas as pd
import numpy as np
import glob
import matplotlib.pyplot as plt

import sys

listdf = []
listclient = []

for f in glob.glob('client*.csv'):
    df = pd.read_csv(f, dtype={'Time':np.long, 'Latency':np.float64})
    
    listdf.append(df)
    listclient.append(f.split('.csv')[0].split('client')[1])

throughput = []
avglatency = []
maxlatency = []
latency_95 = []

for idx, df in enumerate(listdf):
    client_count = listclient[idx]

    total_exc = len(df.index)
    total_nano = df['Time'].iloc[-1] - df['Time'].iloc[0]
    total_sec = total_nano * 1e-9
    throughput.append((client_count, total_exc / float(total_sec)))

    latency = df['Latency'].mean()
    avglatency.append((client_count, latency))

    mlatency = df['Latency'].max()
    maxlatency.append((client_count, mlatency))

    lat_95 = df['Latency'].quantile(q=0.95)
    latency_95.append((client_count, lat_95))

throughput.sort(key=lambda t: int(t[0]))
avglatency.sort(key=lambda t: int(t[0]))
maxlatency.sort(key=lambda t: int(t[0]))
latency_95.sort(key=lambda t: int(t[0]))
print(throughput)
print(avglatency)

plt.figure(0)
plt.bar([n[0] for n in throughput], [n[1] for n in throughput], color='blue')
plt.xlabel('Number of Clients')
plt.ylabel('Throughput')
plt.title('Throughput of Thrift vs. Number of Clients')
plt.savefig('Throughput.jpg')

plt.figure(1)
plt.plot([n[0] for n in avglatency], [n[1] for n in avglatency], 'bo')
plt.xlabel('Number of Clients')
plt.ylabel('Avg Latency (s)')
plt.title('Avg Latency vs. Number of Clients')
plt.savefig('Latency.jpg')

plt.figure(2)
plt.plot([n[0] for n in maxlatency], [n[1] for n in maxlatency], 'bo')
plt.xlabel('Number of Clients')
plt.ylabel('Max Latency (s)')
plt.title('Max Latency vs. Number of Clients')
plt.savefig('Max_Latency.jpg')

plt.figure(3)
plt.plot([n[0] for n in latency_95], [n[1] for n in latency_95], 'bo')
plt.xlabel('Number of Clients')
plt.ylabel('95-th percentile Latency (s)')
plt.title('95-th percentile vs. Number of Clients')
plt.savefig('95_percentile.jpg')