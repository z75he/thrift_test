#include "PerformanceTest.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TSocket.h>
#include <memory>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <csignal>
#include <unistd.h>

using namespace apache::thrift;

bool stop = false;

void worker_thread(int id, int iter, std::string server) {
    auto trans_soc = std::make_shared<transport::TSocket>(server, 9090);
    auto trans = std::make_shared<transport::TFramedTransport>(trans_soc);
    auto proto = std::make_shared<protocol::TBinaryProtocol>(trans);

    std::vector<std::pair<int64_t, double>> delay;

    PerformanceTestClient client(proto);

    trans->open();

    TEST_PACKET_SIMPLE s;

    for (int i = 0; !stop && (i < iter || iter < 0); iter < 0 ? : ++i) {
        auto ts = std::chrono::system_clock::now();
        
        auto t1 = std::chrono::steady_clock::now();
        client.int_return_simple(s, 20l);
        auto t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed_time = t2 - t1;
        // elapsed_time is in second
        delay.emplace_back(ts.time_since_epoch().count(), elapsed_time.count());
    }

    std::ofstream fs;
    char hostname[1024];
    gethostname(hostname, 1023);
    std::string filename = hostname + std::string("_") + std::to_string(id) + "_latency";
    fs.open(filename);
    for (const auto l : delay) {
        fs << l.first << ", " << l.second << std::endl;
    }
}

void SingalHandler(int s) {
    stop = true;
}

int main(int argc, char** argv) {
    int num_thread = 10;
    int num_iter = 10;

    if (argc <= 1) {
        std::cout << "Need at least server address" <<  std::endl;
        return 1;
    }

    std::string server = argv[1];

    if (argc == 3) {
        num_thread = atoi(argv[2]);
    }
    if (argc == 4) {
        num_thread = atoi(argv[2]);
        num_iter = atoi(argv[3]);
    }

    std::signal(SIGTERM, SingalHandler);
    std::signal(SIGINT, SingalHandler);

    std::vector<std::thread> thread_pool;
    for(int i = 0; i < num_thread; ++i) {
        thread_pool.emplace_back(worker_thread, i, num_iter, server);
    }

    for(auto& t : thread_pool) {
        t.join();
    } 
    std::cout<<"Done"<<std::endl;
}